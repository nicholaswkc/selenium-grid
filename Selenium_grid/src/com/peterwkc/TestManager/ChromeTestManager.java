package com.peterwkc.TestManager;

import java.net.MalformedURLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.peterwkc.Manager.WebDriverManager;

public class ChromeTestManager {
	
	private WebDriverManager webDriverManager = new WebDriverManager();
	private WebDriver driver;
	private Map<Long, WebDriver> driverMap = new ConcurrentHashMap<Long, WebDriver>();
    private WebDriverWait wait;
	
	public ChromeTestManager() {
	}
	
    @BeforeClass
    @Parameters(value={"browser"})
    public void setupTest(String browser) throws MalformedURLException {
        System.out.println("BeforeMethod is started. " + Thread.currentThread().getId());
        // Set & Get ThreadLocal Driver with Browser
        
        webDriverManager.createDriver(browser);
        driver = webDriverManager.getDriver();
        driverMap.put(Thread.currentThread().getId(),webDriverManager.getDriver());
        driver = driverMap.get(Long.valueOf(Thread.currentThread().getId()));
        wait = new WebDriverWait(driver, 15);
    }
	
	
	@Test
    public void GOOGLE1() throws Exception {
        WebDriver driver = webDriverManager.getDriver();
        System.out.println("Google0 Test Started! " + Thread.currentThread().getId());
        driver.navigate().to("http://www.google.com");
        System.out.println("Google0 Test's Page title is: " + driver.getTitle() + " " + Thread.currentThread().getId());
        Assert.assertEquals(driver.getTitle(), "Google");
        System.out.println("Google0 Test Ended! " + Thread.currentThread().getId());
    }
 
    @Test
    public void GOOGLE2() throws Exception {
        WebDriver driver = webDriverManager.getDriver();
        System.out.println("Google2 Test Started! " + Thread.currentThread().getId());
        driver.navigate().to("http://www.google.com");
        System.out.println("Google2 Test's Page title is: " + driver.getTitle() + " " + Thread.currentThread().getId());
        Assert.assertEquals(driver.getTitle(), "Google");
        System.out.println("Google2 Test Ended! " + Thread.currentThread().getId());
    }
 
    @Test
    public void GOOGLE3() throws Exception {
        WebDriver driver = webDriverManager.getDriver();
        System.out.println("Google3 Test Started! " + Thread.currentThread().getId());
        driver.navigate().to("http://www.google.com");
        System.out.println("Google3 Test's Page title is: " + driver.getTitle() + " " + Thread.currentThread().getId());
        Assert.assertEquals(driver.getTitle(), "Google");
        System.out.println("Google3 Test Ended! " + Thread.currentThread().getId());
    }
 
    @Test
    public void GOOGLE4() throws Exception {
        WebDriver driver = webDriverManager.getDriver();
        System.out.println("Google4 Test Started! " + Thread.currentThread().getId());
        driver.navigate().to("http://www.google.com");
        System.out.println("Google3 Test's Page title is: " + driver.getTitle() + " " + Thread.currentThread().getId());
        Assert.assertEquals(driver.getTitle(), "Google");
        System.out.println("Google3 Test Ended! " + Thread.currentThread().getId());
    }
    
    @AfterClass
    public void tearDown() throws Exception {
        System.out.println("AfterMethod is started. " + Thread.currentThread().getId());
        webDriverManager.getDriver().quit();
    }
	

}
