package com.peterwkc.TestManager;

import java.net.MalformedURLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.peterwkc.Manager.*;

public class TestManager {
	
	private WebDriverManager webDriverManager = new WebDriverManager();
	private WebDriver driver;
	private Map<Long, WebDriver> driverMap = new ConcurrentHashMap<Long, WebDriver>();
    private WebDriverWait wait;
 
    //Do the test setup
    @BeforeClass
    @Parameters(value={"browser"})
    public void setupTest(String browser) throws MalformedURLException {
        System.out.println("BeforeMethod is started. " + Thread.currentThread().getId());
        // Set & Get ThreadLocal Driver with Browser
        
        webDriverManager.createDriver(browser);
        driver = webDriverManager.getDriver();
        driverMap.put(Thread.currentThread().getId(),webDriverManager.getDriver());
        driver = driverMap.get(Long.valueOf(Thread.currentThread().getId()));
        wait = new WebDriverWait(driver, 15);
    }
 
    @AfterClass
    public void tearDown() throws Exception {
        System.out.println("AfterMethod is started. " + Thread.currentThread().getId());
        webDriverManager.getDriver().quit();
    }
}

	/*
	 * Hub
	 * java -Dwebdriver.gecko.driver= -jar selenium-server-standalone-3.0.1.jar -role hub
	 * 
	 * Node
	 * java -Dwebdriver.gecko.driver= -jar selenium-server-standalone-3.0.1.jar -role node -hub http://localhost:4444/grid/register
	 * 
	 * 
	 * 
	 */





