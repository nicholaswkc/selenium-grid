package com.peterwkc.Listener;

import org.openqa.selenium.WebDriver;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import com.peterwkc.Manager.WebDriverManager;
 
public class InvokedMethodListener implements IInvokedMethodListener {
	
	private WebDriverManager webDriverManager = new WebDriverManager();
	
    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            System.out.println("Test Method BeforeInvocation is started. " + Thread.currentThread().getId());
            String browserName = method.getTestMethod().getXmlTest().getLocalParameters().get("browser");
            
      
            webDriverManager.createDriver(browserName);
        }
    }
 
    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            System.out.println("Test Method AfterInvocation is started. " + Thread.currentThread().getId());

            WebDriver driver = webDriverManager.getDriver();
            if (driver != null) {
                driver.quit();
            }
        }
    }

}